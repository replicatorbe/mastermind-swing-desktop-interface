package model;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public final class ConfigEnvironnement {

	private static volatile ConfigEnvironnement instance = null;

	private String pseudo = "Jérôme Fafchamps";
	private int nbreColonne = 4;
	private int nbreLigne = 9;
	private int nbreCouleur = 6;
	private String theme = "Boule";

	/**
	 * Constructeur de l'objet.
	 */
	private ConfigEnvironnement() {
		// La pr�sence d'un constructeur priv� supprime le constructeur public par d�faut.
		// De plus, seul le singleton peut s'instancier lui m�me.
		super();
	}

	/**
	 * M�thode permettant de renvoyer une instance de la classe ConfigEnvironnement
	 * @return Retourne l'instance du singleton.
	 */
	public final static ConfigEnvironnement getInstance() {
		//Le "Double-Checked Singleton"/"Singleton doublement v�rifi�" permet d'�viter un appel co�teux � synchronized, 
		//une fois que l'instanciation est faite.
		if (ConfigEnvironnement.instance == null) {
			// Le mot-cl� synchronized sur ce bloc emp�che toute instanciation multiple m�me par diff�rents "threads".
			// Il est TRES important.
			synchronized(ConfigEnvironnement.class) {
				if (ConfigEnvironnement.instance == null) {
					ConfigEnvironnement.instance = new ConfigEnvironnement();
				}
			}
		}
		return ConfigEnvironnement.instance;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setNbreColonne(int nbreColonne) {
		this.nbreColonne = nbreColonne;
	}
	public int getNbreColonne() {
		return nbreColonne;
	}
	public void setNbreLigne(int nbreLigne) {
		this.nbreLigne = nbreLigne;
	}
	public int getNbreLigne() {
		return nbreLigne;
	}
	public void setNbreCouleur(int nbreCouleur) {
		this.nbreCouleur = nbreCouleur;
	}
	public int getNbreCouleur() {
		return nbreCouleur;
	}
	public void setTheme(String theme1) {
		this.theme = theme1;
	}
	public String getTheme() {
		return theme;
	}
}
