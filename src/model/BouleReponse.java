package model;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public class BouleReponse extends Boule {

	/**
	 * 
	 */
	private static final long serialVersionUID = 965090735894846444L;

	private int boule;
	
	public BouleReponse(int boule) {
		this.boule=boule;
	}
	
	public void setBoule (int boule) {
		this.boule=boule;
	}

	public int getBoule () {
		return this.boule;
	}

}
