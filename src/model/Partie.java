/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import java.util.Observable;
import java.util.Random;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */
public class Partie extends Observable {

	//configuration du jeu
	private int nbreColonne = ConfigEnvironnement.getInstance().getNbreColonne();
	private int nbreLigne = ConfigEnvironnement.getInstance().getNbreLigne();
	private int nbreCouleur = ConfigEnvironnement.getInstance().getNbreCouleur();

	//ligne/colonne du jeu
	private int ligne = 0;
	private int colonne = 0;
	private int bouleRouge = 0;
	private int bouleBlanche = 0;

	//tableau de la grille principale/ code secret de la solution /couleur differente
	private Boule proposition[][], solution[], couleur[];
	//tableau des boules de reponse � droite
	private BouleReponse reponse[][];

	/**
	 * Permet d'instancier les diff�rents tableaux utilis�s
	 */

	private synchronized void instanceTableau() {
		//tableau de proposition
		setProposition(new Boule[nbreLigne][nbreColonne]);
		//solution aleatoire
		solution = new Boule[nbreColonne];

		setCouleur(new Boule[nbreCouleur+1]);
		setReponse(new BouleReponse[nbreLigne][2]);
	}

	/**
	 * Permet de cr�er le tableau de propositions 
	 */

	private synchronized void createTabProposition() {
		for (int j=0; j<getNbreLigne(); j++) {
			for (int k=0; k<getNbreColonne(); k++) {
				getProposition()[j][k]=new Boule(0,false);      
			}    

		}
	}

	/**
	 * Permet de cr�er un tableau de boules
	 * qui indique le nombre de boules blanches ou rouges
	 * que l'utilisateur re�oit comme info !
	 */

	private synchronized void createTabReponse() {
		for (int j=0; j<getNbreLigne(); j++) {
			for (int k=0; k<2; k++) {
				getReponse()[j][k]=new BouleReponse(0);    
				getReponse()[j][k].setName(""+j+k);
				getReponse()[j][k].setText(""+getReponse()[j][k].getCouleur());
			}    

		}
	}

	/**
	 * Permet de d�finir une solution al�atoirement 
	 * afin que l'utilisateur puisse gagner la partie
	 */
	private synchronized void createtabSol() {
		Random r = new Random();
		for (int j=0; j<getNbreColonne(); j++) {
			int valeur = 1 + r.nextInt(nbreCouleur - 2);
			solution[j]=new Boule(valeur, false);  
		}    
	}


	/**
	 * Diff�rentes couleur dans un tableau (pour le choix des couleurs de l'utilisateur) 
	 */

	private synchronized void createBouleReponse() {
		for (int c = 0; c<=getNbreCouleur(); c++) {
			getCouleur()[c]=new Boule();
			getCouleur()[c].setCouleur(c);
		}
	}

	/**
	 * Reset le tableau des r�ponses 
	 */

	public synchronized void resetTabReponse() {
		for (int j=0; j<getNbreLigne(); j++) {
			for (int k=0; k<2; k++) {
				getReponse()[j][k].setBoule(0);
				getReponse()[j][k].setText(""+getReponse()[j][k].getCouleur());
			}    

		}
	}

	/**
	 * permet de remettre les propositions � 0
	 * envoie a la vue la modification
	 */

	public synchronized void zero() {     
		for (int j=0; j<getNbreLigne(); j++) {
			for (int k=0; k<getNbreColonne(); k++) {
				this.getProposition()[j][k].setEtat(false);
				this.getProposition()[j][k].setCouleur(0);
				ligne=0;
				colonne=0;
			}
		}
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * remet l'�tat des solutions � 0
	 */

	public synchronized void resetEtatSol() {
		for (int j=0; j<getNbreColonne(); j++) {
			solution[j].setEtat(false);    
		}    
	}


	/**
	 * NE DOIS PAS ETRE PRIS EN COMPTE 
	 * UNIQUEMENT TEST SI LA SOLUTION EST EGALE 
	 * AU CHOIX! AFFICHE LA SOLUTION EN CONSOLE
	 */

	public void getSolution() {     
		for (int k=0; k<nbreColonne; k++) {
			System.out.print(solution[k].getCouleur());

		}
	}

	/**
	 * Pas utilis� dans ce MasterMind
	 * @param nb

	public synchronized void getJeu(int nb) {
		getProposition()[ligne][colonne].setCouleur(nb);
		colonne++;
		this.setChanged();
		this.notifyObservers();
	}
	 */
	/**
	 * D�finir quel couleur dans quel colonne du tableau 
	 * de proposition
	 */
	public synchronized void getJeu(int nb, int colonnea) {
		getProposition()[ligne][colonnea].setCouleur(nb);
		colonne++;
		
		
	}


	public synchronized void changementLigne() {
		colonne=0;
		ligne++;
		this.setChanged();
		this.notifyObservers();
	}

	//vide reponse
	public synchronized void videReponse() {
		for (int j=0; j<getNbreLigne(); j++) {
			for (int k=0; k<2; k++) {
				getReponse()[j][k].setCouleur(0);
			}    
		}
		this.setChanged();
		this.notifyObservers();
	}

	//config par d�faut
	public synchronized void configuration() {
		instanceTableau();
		createTabProposition();
		createtabSol();
		createTabReponse();
		createBouleReponse();
	}

	public synchronized void configuration(int colonne, int couleur, int essai) {
		this.setNbreColonne(colonne);
		this.setNbreCouleur(couleur);
		this.setNbreLigne(essai);
		configuration();
	}

	public synchronized void setNbreColonne (int colonne) {
		this.nbreColonne=colonne;
	}

	public synchronized void setNbreLigne(int ligne) {
		this.nbreLigne=ligne;
	}

	public synchronized void setNbreCouleur(int couleur) {
		this.nbreCouleur=couleur;
	}


	public synchronized int verif_boule_blanche() {
		bouleBlanche=0;
		for (colonne=0; colonne<nbreColonne; colonne++) {
			if (this.getProposition()[ligne][colonne].getEtat()==false) {
				for (int i=0;i<nbreColonne;i++) {
					//test si la solution n'a pas encore été testé
					//dans une autre boucle ou celle-ci.				
					if (this.solution[i].getEtat()==false && this.getProposition()[ligne][colonne].getEtat()==false) {
						//si la proposition est la même que la solution
						if (this.getProposition()[ligne][colonne].getCouleur()==this.solution[i].getCouleur()) {
							//on incremente les boules 
							bouleBlanche++;
							//on bloque pour dire qu'on à déjà incrémenté les boules
							this.solution[i].setEtat(true);
							this.getProposition()[ligne][colonne].setEtat(true);
						}
					}     
				}
			}
		}
		return bouleBlanche;
	}

	public synchronized int verif_boule_rouge() {
		bouleRouge=0; 
		for (colonne=0; colonne<nbreColonne; colonne++) {   
			//on verifie si la proposition en cours egale a la solution en cours.
			if (getProposition()[ligne][colonne].getCouleur()==solution[colonne].getCouleur()) {
				//on incremente
				bouleRouge++;  
				solution[colonne].setEtat(true);
				getProposition()[ligne][colonne].setEtat(true);
			}
		}
		return bouleRouge;
	}

	public synchronized void remise_a_zero() {
		//on remet la ligne proposition/solution 0 pour rechercher une nouvelle ligne     
		for (int i=0;i<nbreColonne;i++) {
			this.getProposition()[ligne][i].setEtat(false);
			this.solution[i].setEtat(false);
		}
	}


	public int getNbreColonne() {
		return this.nbreColonne;
	}

	public int getNbreLigne() {
		return this.nbreLigne;
	} 

	public int getNbreCouleur() {
		return this.nbreCouleur;
	}


	public int getLigne() {
		return this.ligne;
	}

	public int getColonne() {
		return this.colonne;
	}

	public  int getBouleRouge() {
		return bouleRouge;
	}

	public int getBouleNoir() {
		return bouleBlanche;
	}

	public synchronized void setReponse(BouleReponse reponse[][]) {
		this.reponse = reponse;
	}

	public BouleReponse[][] getReponse() {
		return reponse;
	}

	public synchronized void setProposition(Boule proposition[][]) {
		this.proposition = proposition;
	}

	public synchronized Boule[][] getProposition() {
		return proposition;
	}

	public synchronized void setCouleur(Boule couleur[]) {
		this.couleur = couleur;
	}

	public synchronized Boule[] getCouleur() {
		return couleur;
	}

}