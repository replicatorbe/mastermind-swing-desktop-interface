
package vue;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public class Splash extends JFrame{


	private static final long serialVersionUID = -6941788870721762852L;
	private PanneauIntro pan = new PanneauIntro();
	private JLabel j = new JLabel("MasterMind");

	public Splash(){

		this.setTitle("MasterMind");
		this.setSize(300, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setContentPane(pan);
		this.setAlwaysOnTop(true);
		//impossible de redimensionner
		this.setResizable(false);
		//retire le cadre
		this.setUndecorated(true);
		pan.add(j);
		this.setVisible(true);

		go();
	}

	private void go(){

		for(int i = -30; i < pan.getWidth(); i++) {
			int x = pan.getPosX(), y = pan.getPosY();
			x++;
			y++;
			pan.setPosX(x);
			pan.setPosY(y);
			pan.repaint();  

			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}       
}
