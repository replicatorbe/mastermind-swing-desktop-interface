
package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import ctrl.*;
import model.*;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */
public class JeuGraphique implements Observer {

	private Partie partie;
	private JFrame fenetre = new JFrame();//fenetre
	private JLayeredPane pane = new JLayeredPane();

	private JPanel pan2 = new JPanel();//nbre de choix
	private PanneauJeu panneau = new PanneauJeu();//plateau

	private JPanel pan = new JPanel();//plateau solution
	private JPanel menu = new JPanel(new GridLayout(10,0)); //panel du menu

	private static int x = 50;
	private static int y = 50;
	private static String dossier = "skins/";
	private static ImageIcon pionNoir;
	private static ImageIcon pionVertClair;
	private static ImageIcon pionRouge;
	private static ImageIcon pionMauve;
	private static ImageIcon pionMauveFonce;
	private static ImageIcon pionOrange;
	private static ImageIcon pionVert;
	private static ImageIcon pionBlanc;
	private static ImageIcon gPionNoir;
	private static ImageIcon gPionVertClair;
	private static ImageIcon gPionRouge;
	private static ImageIcon gPionMauve;
	private static ImageIcon gPionMauveFonce;
	private static ImageIcon gPionOrange;
	private static ImageIcon gPionVert;

	private Image icone = Toolkit.getDefaultToolkit().getImage("petit.png");
	private Thread t = new Thread(panneau);
	private JMenuBar mb = new JMenuBar();
	private List<Boule> listLabel = new ArrayList<Boule>();
	private JLabel status;


	/**
	 * Permet d'instancier les images
	 */

	private static void instance_image() {
		pionNoir = new ImageIcon(transforme_image("boulenoir.png"));
		pionVertClair = new ImageIcon(transforme_image("bouleverteclair.png"));
		pionRouge = new ImageIcon(transforme_image("boulerouge.png"));
		pionMauve = new ImageIcon(transforme_image("boulemauve.png"));
		pionMauveFonce = new ImageIcon(transforme_image("boulemauvefonce.png"));
		pionOrange = new ImageIcon(transforme_image("bouleorange.png"));
		pionVert = new ImageIcon(transforme_image("bouleverte.png"));
		pionBlanc = new ImageIcon(transforme_image("bouleblanche.png"));
		gPionNoir = new ImageIcon(transforme_image("boulenoir.png"));
		gPionVertClair = new ImageIcon(transforme_image("gbouleverteclair.png"));
		gPionRouge = new ImageIcon(transforme_image("gboulerouge.png"));
		gPionMauve = new ImageIcon(transforme_image("gboulemauve.png"));
		gPionMauveFonce = new ImageIcon(transforme_image("gboulemauvefonce.png"));
		gPionOrange = new ImageIcon(transforme_image("gbouleorange.png"));
		gPionVert = new ImageIcon(transforme_image("gbouleverte.png"));
	}

	/**
	 * Permet de transformer une image � la taille d�sir�e
	 * @param chemin
	 * @return pion
	 */
	private static Image transforme_image(String chemin) {
		ImageIcon pionOne = new ImageIcon(Toolkit.getDefaultToolkit().getImage(dossier + ConfigEnvironnement.getInstance().getTheme() + "/" + chemin)); 
		Image pion = pionOne.getImage().getScaledInstance(x, y, Image.SCALE_DEFAULT);	
		return pion;
	}

	/**
	 * D�finir le modele a la vue.
	 * @param modele
	 */
	public void setModele(Partie modele) {
		this.partie=modele;
		this.partie.addObserver(this);
		this.affiche_fenetre();
	}

	/**
	 * Fenetre principale du jeu
	 */

	private void affiche_fenetre() {     
		fenetre.setSize(600,600);
		fenetre.setResizable(false);
		fenetre.setLocationRelativeTo(null);
		fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetre.setIconImage(icone);
		fenetre.setContentPane(panneau);
		fenetre.setTitle("Good game avec MasterMind");

		pan2.setOpaque(false);
		menu.setOpaque(false);
		pan.setOpaque(false);
		mb.setOpaque(false);

		Border blackline = BorderFactory.createLineBorder(Color.RED,1); 
		pan2.setBorder(blackline);
		//	pan.setLayout(new GridLayout(partie.getNbreLigne(),partie.getNbreColonne(),-90,-59));


		//	pan2.setLayout(new GridLayout(partie.getNbreLigne(),2,0,-59));
		panneau.setLayout(new BorderLayout());

		//	mb.add(Box.createHorizontalGlue());


		mb.setBorderPainted(false);
		JMenu menu = new JMenu("Partie");
		JMenu menu3 = new JMenu("Th�me");
		mb.add(menu);
		menu.setForeground(Color.white);
		menu3.setForeground(Color.white);
		mb.add(menu3);


		ajoute_item_menu("Nouvelle partie", menu);
		ajoute_item_menu("Valider", menu);
		ajoute_item_menu("Fermer la partie", menu);
		ajoute_item_menu("Anneau", menu3);
		ajoute_item_menu("Bouteille", menu3);
		ajoute_item_menu("Canette", menu3);
		ajoute_item_menu("Cone", menu3);
		ajoute_item_menu("Cube", menu3);
		ajoute_item_menu("Triangle", menu3);
		ajoute_item_menu("Boule", menu3);

		/**
		 * barre d'�tat
		 */

		status = new JLabel();
		status.setBorder(new EtchedBorder());
		status.setForeground(Color.black);
		status.setOpaque(true);
		status.setText("MasterMind DGUI - Bienvenue " + ConfigEnvironnement.getInstance().getPseudo());
		//Fin d'�tat


		panneau.add(mb, BorderLayout.PAGE_START);
		//pane.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		panneau.add(pane, BorderLayout.CENTER);
		//panneau.add(pan2, BorderLayout.LINE_END);
		panneau.add(status, BorderLayout.PAGE_END);
		//	pan.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT); 	

		instance_image();
		affiche_grille(true);
		affiche_boule(true);
		affiche_reponse(true);
		t.start();
		fenetre.addKeyListener(new MenuItemCtrl(this,partie));
		fenetre.setVisible(true);
	}


	/**
	 * Cr�ation des items des menus
	 * @param texte
	 * @param c
	 */

	private void ajoute_item_menu(String texte, Container c) {
		JMenuItem mi = new JMenuItem();
		mi.setText(texte);
		mi.setEnabled(true);
		mi.addActionListener(new MenuItemCtrl(this,partie));
		c.add(mi);
	}

	/**
	 * Permet de changer le skin (mini-moteur de template)
	 * @param theme
	 */

	public static void setSkin(String theme) {
		instance_image();
	}


	/**
	 * Refresh l'�cran
	 */

	public void refresh() {
		affiche_grille(false);
		affiche_boule(false);
		affiche_reponse(false);
		arrayFresh();
	}

	/**
	 * SETTEXT dans barre d'�tat
	 */

	public void setTextBar(String texte) {
		this.status.setText("MasterMind DGUI " + texte);
	}


	/**
	 * Affiche la grille des pions de proposition (pions noirs dans le jeu)
	 * @param etat 
	 */

	private void affiche_grille(Boolean etat) {	
		for(int i = 0; i<partie.getNbreLigne(); i++){
			for (int j= 0; j<partie.getNbreColonne(); j++) {
				partie.getProposition()[i][j].setIcon(mettrePion(partie.getProposition()[i][j].getCouleur()));
				partie.getProposition()[i][j].setName("0");
				partie.getProposition()[i][j].setBounds((70+j*80), i*48, x, y);
				partie.getProposition()[i][j].setDrag(false);
				if (etat) pane.add(partie.getProposition()[i][j],Integer.valueOf(5));
			}
		} 
	}

	/**
	 * Grille des r�ponses (droite de l'�cran)
	 */

	private void affiche_reponse(Boolean etat) {

		for(int i = 0; i<partie.getNbreLigne(); i++){
			for (int j= 0; j<2; j++) {
				if (j==0) {
					partie.getReponse()[i][j].setIcon(grasPion(3));	
				} else {
					partie.getReponse()[i][j].setIcon(mettrePion(7));
				}
				partie.getReponse()[i][j].setDrag(false);
				partie.getReponse()[i][j].setForeground(Color.WHITE);
				//	partie.getReponse()[i][j].setHorizontalAlignment(SwingConstants.CENTER);   
				//		partie.getReponse()[i][j].setVerticalAlignment(SwingConstants.NORTH);
				partie.getReponse()[i][j].setBounds((425+j*60), i*48, x+15, y);	
				if (etat) pane.add(partie.getReponse()[i][j],Integer.valueOf(3));

			}
		} 
	}

	/**
	 * Permet de fermer la fen�tre
	 */

	public void dispose() {
		this.fenetre.dispose();
	}


	/**
	 * Permet d'afficher les boules de couleur que le joueur peut selectionner
	 * et mettre dans la grille des propositions.
	 * @param on
	 */

	private void affiche_boule(boolean on) {
		for (int c = 1; c<=partie.getNbreCouleur(); c++) {

			if (c==1){
				partie.getCouleur()[c].setIcon(pionVertClair);
			} else if (c==2) {
				partie.getCouleur()[c].setIcon(pionVert);	
			} else if (c==3) {
				partie.getCouleur()[c].setIcon(pionRouge);
			} else if (c==4){
				partie.getCouleur()[c].setIcon(pionMauve);
			} else if (c==5) {
				partie.getCouleur()[c].setIcon(pionOrange);
			} else if (c==6) {
				partie.getCouleur()[c].setIcon(pionMauveFonce);
			}

			partie.getCouleur()[c].setName(""+c);

			//x//y//h//l
			partie.getCouleur()[c].setDrag(true);
			partie.getCouleur()[c].setBounds((c*65), 470, x, y);

			if (on) {
				listLabel.add(partie.getCouleur()[c]);
				partie.getCouleur()[c].addMouseListener(new JeuCtrl(partie,this));
				partie.getCouleur()[c].addMouseMotionListener(new JeuCtrl(partie,this));
				pane.add(partie.getCouleur()[c], Integer.valueOf(12));
			}


		}
	}

	/**
	 * Permet de mettre dans la liste des gagnants (nombre de boules blanches, nombre de boules rouges)
	 * @param action
	 * @param boule
	 */

	public void mettreBoule(int action, int boule) {
		if (action ==0) {

			partie.getReponse()[partie.getLigne()][action].setText(""+boule);
		} else if (action ==1){

			partie.getReponse()[partie.getLigne()][action].setText(""+boule);		
		}
	}

	/**
	 * Permet de cr�er une boule pour mettre dans la grille de proposition
	 * @param r (position)
	 * @param nom (numero de couleur)
	 * @return
	 */

	public Boule boulePartie(Rectangle r, int nom) {
		Boule b = new Boule(0,false);
		b.setDrag(true);
		b.setBounds(r); 
		b.setName(""+nom);
		b.setCouleur(nom);
		b.setIcon(mettrePion(nom));
		b.setVisible(true);
		b.addMouseMotionListener(new JeuCtrl(partie,this));
		b.addMouseListener(new JeuCtrl(partie,this));
		listLabel.add(b);
		pane.add(b, Integer.valueOf(15));
		return b;
	}

	/**
	 * Permet de d�tecter s'il y a une intersection entre nos propositions et nos boules
	 * mises dans la grille par la souris. Si c'est le cas, on n'affiche plus ces boules
	 */

	public void detectionEcran() {

		for(int i = 0; i<partie.getNbreLigne(); i++){
			for (int j= 0; j<partie.getNbreColonne(); j++) {
				for (int k= 0; k<listLabel.size(); k++) {

					if (partie.getProposition()[i][j].getBounds().intersects(listLabel.get(k).getBounds())) {
						listLabel.get(k).setVisible(false);
					}
				}

			}
		}
	}

	/**
	 * Permet de s'assurer que le tableau des nouvelles boules se remet � jour
	 * rapidement lors d'un nouveau theme choisi par l'utilisateur
	 */

	public void arrayFresh() {

		for(int i = 0; i<listLabel.size(); i++){
			listLabel.get(i).setIcon(mettrePion(listLabel.get(i).getCouleur()));
		}

	}

	/**
	 * insere dans le pannel
	 * @param pan
	 * @param c
	 */

	private void setPannel(Boule pan, Container c) {
		c.add(pan);
	}


	/**
	 * M�thode permettant de choisir un pion un peu plus fonc�
	 * (requis pour faire un magnifique rollover/effet utilisateur)
	 * @param c
	 * @return
	 */

	public ImageIcon grasPion(int c) {

		switch(c){ 
		case 0 :
			return gPionNoir;
		case 1 : 
			return gPionVertClair;
		case 2 : 
			return gPionVert;
		case 3 : 
			return gPionRouge;
		case 4 : 
			return gPionMauve;
		case 5 : 
			return gPionOrange;
		case 6 : 
			return gPionMauveFonce;
		default: 
			return null;
		} 
	}

	/**
	 * Choisir la couleur du pion selon le num�ro de couleur
	 * @param c
	 * @return
	 */

	public ImageIcon mettrePion(int c) {
		switch(c){ 
		case 0 :
			return pionNoir;
		case 1 : 
			return pionVertClair;
		case 2 : 
			return pionVert;
		case 3 : 
			return pionRouge;
		case 4 : 
			return pionMauve;
		case 5 : 
			return pionOrange;
		case 6 : 
			return pionMauveFonce;
		case 7 : 
			return pionBlanc;

		default: 
			return null;
		} 
	}

	/* Design pattern Observer-Observable
	 * (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */

	@Override
	public void update(Observable o, Object arg) {
		affiche_grille(false);   
		affiche_reponse(false);
	}

	/*
	 * Fenetre de dialogue d'alerte informant que la partie est gagn�e
	 */

	public int win() {
		secouer(fenetre, 20, 2);;
		int rep = JOptionPane.showConfirmDialog(null, 
				"F�licitation, vous avez gagn� !!! Voulez-vous rejouer ?", "Gagn� !", JOptionPane.YES_NO_OPTION);

		if(rep==JOptionPane.YES_OPTION) {

			fenetre.dispose();
			return 1;

		} else if (rep==JOptionPane.NO_OPTION) {
			fenetre.dispose();
			return 0;

		} else { win(); return 0;}
	}

	/*
	 * Permet de secouer la fen�tre. 
	 */

	private void secouer (Component c, int vitesse, int decallage) {
		int pos_x=c.getBounds().getLocation().x;
		int pos_y=c.getBounds().getLocation().y;

		int [] po={0,decallage};
		//fait bouger la fen�tre 14 fois
		for (int i=0;i<14;i++)
		{
			//fait une division
			//si le reste de la division (mod) est 0, position initial
			//sinon position + decallage
			c.setBounds(pos_x+po[i%2],pos_y+po[i%2],c.getBounds().getSize().width,c.getBounds().getSize().height);
			try
			{
				Thread.sleep(vitesse);
			}
			catch(Exception e12){}
		}
	}

	/**
	 * Fenetre de dialogue informant que la partie est perdue.
	 * @return 0 ou 1 
	 */

	public int lose () {
		int rep = JOptionPane.showConfirmDialog(null, 
				"Dommage, vous avez perdu ! Voulez-vous rejouer ?", "Perdu !", JOptionPane.YES_NO_OPTION);
		if(rep==JOptionPane.YES_OPTION) {
			fenetre.dispose();
			return 1;
		} else if (rep==JOptionPane.NO_OPTION) {
			fenetre.dispose();
			return 0;
		} else { 
			lose();
			return 0; 
		}

	}

	public void nombreEleve () {
		JOptionPane.showConfirmDialog(null, "Vous ne pouvez pas choisir cette couleur dans votre configuration!", "Perdu !", JOptionPane.NO_OPTION);
	}

	/**
	 * Fenetre de dialog -> action impossible de changer de th�me lors d'un d�but de colonne
	 */

	public void alerteSkin () {
		JOptionPane.showMessageDialog(null, "Veuillez d'abord valider l'action du jeu en cours...", "Alerte!!!", JOptionPane.WARNING_MESSAGE);
	}

	public void alerteTimers() {
		// TODO Auto-generated method stub
		JOptionPane.showMessageDialog(null, "Vous avez d�j� choisi un level!", "Alerte!!!", JOptionPane.WARNING_MESSAGE);

	}
}
