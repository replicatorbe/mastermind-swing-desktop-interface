package vue;

import java.awt.*;

import javax.swing.*;

import ctrl.ConfigurationCtrl;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public class FenetreConfiguration extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image icone = Toolkit.getDefaultToolkit().getImage("petit.png");
	private PanneauFond panneau = new PanneauFond();//plateau
	private JPanel panel = new JPanel(new SpringLayout());
	public JTextField textFieldPseudo = new JTextField("");
	JLabel labelPseudo = new JLabel("Pseudo: ");

	public JTextField textFieldCol = new JTextField("");
	JLabel labelCol = new JLabel("Nombre de colonnes: ");
	
	public JTextField textFieldEssai = new JTextField("");
	JLabel labelEssai = new JLabel("Nombre d'essais: ");
	
	public JTextField textFieldCouleur = new JTextField("");
	JLabel labelCouleur = new JLabel("Nombre de couleurs: ");

	public JComboBox combo = new JComboBox();
	JLabel theme = new JLabel("Theme: ");
	
	JLabel labelSave = new JLabel("");
	JButton save = new JButton("Enregistrer");
	
	public FenetreConfiguration() {

		this.setSize(600,600);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setIconImage(icone);
		this.setContentPane(panneau);
		this.addWindowListener(new ConfigurationCtrl(this));
		
		
		panneau.setLayout(new BorderLayout());
		
		textFieldEssai.setOpaque(false);
		textFieldPseudo.setOpaque(false);
		textFieldCol.setOpaque(false);
		combo.setOpaque(false);
		textFieldCouleur.setOpaque(false);
		labelSave.setVisible(false);
		
		labelPseudo.setForeground(Color.RED);
		labelCol.setForeground(Color.RED);
		labelEssai.setForeground(Color.RED);
		labelCouleur.setForeground(Color.RED);
		theme.setForeground(Color.RED);
		textFieldPseudo.setForeground(Color.RED);
		textFieldCol.setForeground(Color.RED);
		textFieldEssai.setForeground(Color.RED);
		textFieldCouleur.setForeground(Color.RED);
		combo.setForeground(Color.RED);
		save.setForeground(Color.RED);
		
		save.addActionListener(new ConfigurationCtrl(this));
		
		combo.addItem("Anneau");
		combo.addItem("Bouteille");
		combo.addItem("Canette");
		combo.addItem("Cone");
		combo.addItem("Cube");
		combo.addItem("Triangle");
		combo.addItem("Boule");
		
		panel.setOpaque(false);
		panel.add(labelPseudo);
		panel.add(textFieldPseudo);
		panel.add(labelCol);
		panel.add(textFieldCol);
		panel.add(labelEssai);
		panel.add(textFieldEssai);
		panel.add(labelCouleur);
		panel.add(textFieldCouleur);
		panel.add(theme);
		panel.add(combo);
		panel.add(labelSave);
		panel.add(save);
		
		/*
		 * dans le cadre de notre sophistication de notre interface
		 * d'utilisateur > appuyer enter pour valider le formulaire et non �tre
		 * obliger d'utiliser la souris
		 */
		labelPseudo.addKeyListener(new ConfigurationCtrl(this));
		labelCol.addKeyListener(new ConfigurationCtrl(this));
		labelCouleur.addKeyListener(new ConfigurationCtrl(this));
		labelEssai.addKeyListener(new ConfigurationCtrl(this));
		labelSave.addKeyListener(new ConfigurationCtrl(this));
		textFieldCol.addKeyListener(new ConfigurationCtrl(this));
		textFieldCouleur.addKeyListener(new ConfigurationCtrl(this));
		textFieldEssai.addKeyListener(new ConfigurationCtrl(this));
		textFieldPseudo.addKeyListener(new ConfigurationCtrl(this));
		save.addKeyListener(new ConfigurationCtrl(this));
		
		this.panneau.add(panel, BorderLayout.CENTER);
		
		this.setTitle("Configuration du MasterMind");
		this.creerGrilleCompacte(panel, 6, 2, 5, 5, 5, 5);
		this.setVisible(true);


	}

	
	public JTextField getTextField() {
		return textFieldPseudo;
	}


	public void setTextField(JTextField textField) {
		this.textFieldPseudo = textField;
	}


	public JLabel getLabel() {
		return labelPseudo;
	}


	public void setLabel(JLabel label) {
		this.labelPseudo = label;
	}


	public JTextField getTextField1() {
		return textFieldCol;
	}


	public void setTextField1(JTextField textField1) {
		this.textFieldCol = textField1;
	}


	public JLabel getLabel1() {
		return labelCol;
	}


	public void setLabelCol(JLabel label1) {
		this.labelCol = label1;
	}


	public JTextField getTextFieldEssai() {
		return textFieldEssai;
	}


	public void setTextFieldEssai(JTextField textFieldEssai) {
		this.textFieldEssai = textFieldEssai;
	}


	public JLabel getLabelEssai() {
		return labelEssai;
	}


	public void setLabelEssai(JLabel labelEssai) {
		this.labelEssai = labelEssai;
	}


	public JTextField getTextFieldCouleur() {
		return textFieldCouleur;
	}


	public void setTextFieldCouleur(JTextField textFieldCouleur) {
		this.textFieldCouleur = textFieldCouleur;
	}


	public JLabel getLabel1Couleur() {
		return labelCouleur;
	}


	public void setLabelCouleur(JLabel label1Couleur) {
		this.labelCouleur = label1Couleur;
	}


	public JComboBox getCombo() {
		return combo;
	}


	public void setCombo(JComboBox combo) {
		this.combo = combo;
	}


	public JLabel getTheme() {
		return theme;
	}


	public void setTheme(JLabel theme) {
		this.theme = theme;
	}


	public void creerGrilleCompacte(JPanel panneau, int lignes, int colonnes, int initialX, int initialY, int espacementX, int espacementY) {

		SpringLayout gestionnaire = null;
		if (panneau.getLayout() instanceof SpringLayout)
			gestionnaire = (SpringLayout) panneau.getLayout();
		else throw new ClassCastException();

		Spring x = Spring.constant(initialX);
		for (int col = 0; col < colonnes; col++) {
			Spring largeur = Spring.constant(0);
			for (int ligne = 0; ligne < lignes; ligne++) {
				largeur = Spring.max(largeur, getConstraintesComposant(ligne,
						col, panneau, colonnes).getWidth());
			}
			for (int ligne = 0; ligne < lignes; ligne++) {
				SpringLayout.Constraints contraintes = getConstraintesComposant(
						ligne, col, panneau, colonnes);
				contraintes.setX(x);
				contraintes.setWidth(largeur);
			}
			x = Spring
			.sum(x, Spring.sum(largeur, Spring.constant(espacementX)));
		}
		Spring y = Spring.constant(initialY);
		for (int ligne = 0; ligne < lignes; ligne++) {
			Spring hauteur = Spring.constant(0);
			for (int colonne = 0; colonne < colonnes; colonne++) {
				hauteur = Spring.max(hauteur, getConstraintesComposant(ligne,
						colonne, panneau, colonnes).getHeight());
			}
			for (int c = 0; c < colonnes; c++) {
				SpringLayout.Constraints contraintes = getConstraintesComposant(
						ligne, c, panneau, colonnes);
				contraintes.setY(y);
				contraintes.setHeight(hauteur);
			}
			y = Spring
			.sum(y, Spring.sum(hauteur, Spring.constant(espacementY)));
		}
		SpringLayout.Constraints contraintesPanneau = gestionnaire
		.getConstraints(panneau);
		contraintesPanneau.setConstraint(SpringLayout.SOUTH, y);
		contraintesPanneau.setConstraint(SpringLayout.EAST, x);
	}

	private SpringLayout.Constraints getConstraintesComposant(
			int ligne,
			int colonne,
			JPanel parent,
			int colonnes) {
		SpringLayout gestionnaire = (SpringLayout) parent.getLayout();
		Component composant = parent.getComponent(
				ligne * colonnes + colonne);
		return gestionnaire.getConstraints(composant);
	}

	public void alerteColonnes () {
		JOptionPane.showMessageDialog(null, "Le nombre de colonnes n'est pas valide !", "Alerte!!!", JOptionPane.WARNING_MESSAGE);
	}
	public void alerteEssais () {
		JOptionPane.showMessageDialog(null, "Le nombre d'essais n'est pas valide !", "Alerte!!!", JOptionPane.WARNING_MESSAGE);
	}
	public void alerteCouleurs () {
		JOptionPane.showMessageDialog(null, "Le nombre de couleurs n'est pas valide !", "Alerte!!!", JOptionPane.WARNING_MESSAGE);
	}

}