/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class PanneauIntro extends JPanel {

	private String filePath="petit.png";
	private static final long serialVersionUID = 5372306259921890578L;
	private int posX = -10;
	private int posY = -10;


	public void paintComponent(Graphics g){
		BufferedImage image;

		try {
			image = ImageIO.read(new File(filePath));
			g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);  
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}                                


		if (this.getPosX()>=this.getWidth()) {
			posX = 5;
			posY = 5;
		} 


		g.setColor(Color.white);
		g.fillOval(posX, posY, 50, 50);		
		g.setColor(Color.CYAN);
		g.fillOval(posX+35, posY+65, 50, 50);
		g.setColor(Color.blue);
		g.fillOval(posX+90, posY+95, 60, 60);  
	}


	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

}
