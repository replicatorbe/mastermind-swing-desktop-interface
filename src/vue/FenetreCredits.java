package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public class FenetreCredits extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2429368307451703289L;
	private JPanel panel = new JPanel(new BorderLayout());
	private PanneauJeu panel3 = new PanneauJeu();
	private JLabel jintro = new JLabel("MasterMind r�alis� par : ");
	private JLabel jjerome = new JLabel("J�r�me Fafchamps");
	
	private JPanel panel2 = new JPanel();
	private Image icone = Toolkit.getDefaultToolkit().getImage("petit.png");
	
	
	public FenetreCredits() {
		
		this.setTitle("MasterMind");
		this.setSize(300,300);
		this.setResizable(false);
		this.setContentPane(panel);
		this.panel2.setOpaque(false);
		this.setIconImage(icone);
		this.jintro.setForeground(Color.WHITE);
		this.jjerome.setForeground(Color.WHITE);
		
		this.panel.add(panel3, BorderLayout.CENTER);
		this.panel3.add(panel2);
		this.panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
		this.panel2.add(jintro);
		this.panel2.add(Box.createRigidArea(new Dimension(0,20)));
		this.panel2.add(jjerome);
		
		this.setVisible(true);
	}

}
