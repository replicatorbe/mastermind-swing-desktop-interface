package vue;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public class PanneauJeu extends JPanel implements Runnable {

	private static final long serialVersionUID = -772873848773004004L;
	private String filePath="fonda.png";

	public void run() {
		// TODO Auto-generated method stub
		try {
			while (true) {
				Thread.sleep(2000);
				filePath="fonda.png";
				this.repaint();
				Thread.sleep(2000);
				filePath="fondc.png";
				this.repaint();
				Thread.sleep(1000);
				filePath="fonda.png";
				this.repaint();
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void paintComponent(Graphics g) {

		try {
			BufferedImage image = ImageIO.read(new File(filePath));             
			g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);                                       

		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

}
