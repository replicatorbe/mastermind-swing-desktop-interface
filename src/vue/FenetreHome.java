
package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import ctrl.*;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */
public class FenetreHome extends JFrame {

	private static final long serialVersionUID = 646735462947799696L;
	private JPanel panel = new JPanel(new BorderLayout());
	private JPanel panel2 = new JPanel();
	private PanneauHome panel3 = new PanneauHome();
	private Thread t1 = new Thread(panel3);
	private JPanel panel4 = new JPanel(new GridLayout(0,4));
	private Image icone = Toolkit.getDefaultToolkit().getImage("petit.png");


	public FenetreHome() {
		//titre
		this.setTitle("MasterMind");
		this.setSize(600,600);
		this.setResizable(false);
		this.setContentPane(panel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setIconImage(icone);;
		panel.setOpaque(false);
		panel4.setOpaque(false);
		panel.add(panel3, BorderLayout.CENTER);
		panel3.add(panel4);
		panel4.add(panel2);
		panel2.setLayout(new BoxLayout(panel2,BoxLayout.PAGE_AXIS)); 
		//on rend transparent le panel
		panel2.setOpaque(false);
		panel2.add(Box.createVerticalStrut(150));
		addAButton("Jouer", panel2);
		panel2.add(Box.createVerticalStrut(30));
		addAButton("Configuration", panel2);
		panel2.add(Box.createVerticalStrut(30));
		addAButton("Credit", panel2);
		panel2.add(Box.createVerticalStrut(30));
		addAButton("Fermer", panel2);
		t1.start();
		this.setVisible(true);
	}


	/**
	 * ajouter un bouton dans le menu
	 * @param text
	 * @param container
	 */
	private static void addAButton(String text, Container container) {    
		JButton button = new JButton(text);        
		button.setPreferredSize(new Dimension(110,40));
		button.setMaximumSize(button.getPreferredSize());
		button.addActionListener(new HomeCtrl());
		button.addMouseListener(new HomeCtrl());
		button.setOpaque(true);
		button.setBorderPainted(true);
		button.setForeground(Color.red);
		button.setContentAreaFilled(false);
		button.setRolloverEnabled(true);
		button.setCursor(Cursor.getPredefinedCursor(12));
		container.add(button);  
	}
}   

