package ctrl;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import model.ConfigEnvironnement;

import vue.FenetreConfiguration;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public class ConfigurationCtrl implements ActionListener, WindowListener, KeyListener {


	private FenetreConfiguration fc;

	public ConfigurationCtrl(FenetreConfiguration vue) {
		this.fc=vue;
	}

	private void checkConfig() {
		if(!fc.textFieldCol.getText().equalsIgnoreCase("") && !fc.textFieldCol.getText().equalsIgnoreCase("1") && !fc.textFieldCol.getText().equalsIgnoreCase("2") && !fc.textFieldCol.getText().equalsIgnoreCase("3") && !fc.textFieldCol.getText().equalsIgnoreCase("4") && !fc.textFieldCol.getText().equalsIgnoreCase("5") && !fc.textFieldCol.getText().equalsIgnoreCase("6") && !fc.textFieldCol.getText().equalsIgnoreCase("7") && !fc.textFieldCol.getText().equalsIgnoreCase("8") && !fc.textFieldCol.getText().equalsIgnoreCase("9")) {
			fc.alerteColonnes();
			return;
		}
		else if(!fc.textFieldEssai.getText().equalsIgnoreCase("") && !fc.textFieldEssai.getText().equalsIgnoreCase("1") && !fc.textFieldEssai.getText().equalsIgnoreCase("2") && !fc.textFieldEssai.getText().equalsIgnoreCase("3") && !fc.textFieldEssai.getText().equalsIgnoreCase("4") && !fc.textFieldEssai.getText().equalsIgnoreCase("5") && !fc.textFieldEssai.getText().equalsIgnoreCase("6") && !fc.textFieldEssai.getText().equalsIgnoreCase("7") && !fc.textFieldEssai.getText().equalsIgnoreCase("8") && !fc.textFieldEssai.getText().equalsIgnoreCase("9")) {
			fc.alerteEssais();
			return;
		}
		else if(!fc.textFieldCouleur.getText().equalsIgnoreCase("") && !fc.textFieldCouleur.getText().equalsIgnoreCase("3") && !fc.textFieldCouleur.getText().equalsIgnoreCase("4") && !fc.textFieldCouleur.getText().equalsIgnoreCase("5") && !fc.textFieldCouleur.getText().equalsIgnoreCase("6")) {
			fc.alerteCouleurs();
			return;
		}
		if(!fc.textFieldCol.getText().equalsIgnoreCase("")) {
			ConfigEnvironnement.getInstance().setNbreColonne(Integer.parseInt(fc.textFieldCol.getText()));
		}
		if(!fc.textFieldEssai.getText().equalsIgnoreCase("")) {
			ConfigEnvironnement.getInstance().setNbreLigne(Integer.parseInt(fc.textFieldEssai.getText()));
		}
		if ((!fc.textFieldCouleur.getText().equalsIgnoreCase("") && (!fc.textFieldCouleur.getText().equalsIgnoreCase("1")))) {
			ConfigEnvironnement.getInstance().setNbreCouleur(Integer.parseInt(fc.textFieldCouleur.getText()));
		}
		if(!fc.textFieldPseudo.getText().equalsIgnoreCase("")) {
			ConfigEnvironnement.getInstance().setPseudo(fc.textFieldPseudo.getText());
		}
		if(!fc.combo.getSelectedItem().toString().equalsIgnoreCase("")) {
			ConfigEnvironnement.getInstance().setTheme(fc.combo.getSelectedItem().toString());

		}
		fc.dispose();
	}


	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {

		/**
		 * mise des elements dans la fenetre de configuration
		 */
		// TODO Auto-generated method stub
		fc.textFieldCouleur.setText(""+ConfigEnvironnement.getInstance().getNbreCouleur());
		fc.textFieldEssai.setText(""+ConfigEnvironnement.getInstance().getNbreLigne());
		fc.textFieldPseudo.setText(""+ConfigEnvironnement.getInstance().getPseudo());
		fc.textFieldCol.setText(""+ConfigEnvironnement.getInstance().getNbreColonne());


		for(int i = 0 ; i<fc.combo.getItemCount() ; i++){
			System.out.println(fc.combo.getItemAt(i));

			if (fc.combo.getItemAt(i).equals(ConfigEnvironnement.getInstance().getTheme())) {
				fc.combo.setSelectedIndex(i);
			}

		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String source = e.getActionCommand();
		if (source == "Enregistrer"){
			checkConfig();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_ENTER) {
			Toolkit.getDefaultToolkit().beep();
			checkConfig();
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}



}
