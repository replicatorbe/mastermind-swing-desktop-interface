package ctrl;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import model.Boule;
import model.Partie;

import commun.ExceptionJeu;
import vue.JeuGraphique;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public class JeuCtrl implements MouseListener, MouseMotionListener {

	private Partie partie;
	private JeuGraphique vue;


	public JeuCtrl (Partie partie, JeuGraphique vue) {
		this.partie = partie;
		this.vue = vue;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}


	@Override
	public void mousePressed(MouseEvent e) {

		Component component = e.getComponent();

		if ((component instanceof Boule) && (component.getName() != "0")) {

			Boule b = (Boule) component;
			if (b.getDrag())
				vue.boulePartie(e.getComponent().getBounds(),Integer.parseInt(e.getComponent().getName()));			

		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Component component = e.getComponent();

		if ((component instanceof Boule) && (component.getName() != "0")) {

			Boule b = (Boule)component;

			int detect = detectionv(e);

			if (detect!=250)
			b.setBounds(partie.getProposition()[partie.getLigne()][detect].getBounds().x, partie.getProposition()[partie.getLigne()][detect].getBounds().y, 50, 50);
		
			if (detect <= partie.getNbreColonne()) {

				try {
					if (b.getDrag()) {
						getJeu(Integer.parseInt(component.getName()), detect);
						b.setDrag(false);
						b.setPosition(detect);
					//	b.setVisible(false);
					} else {
						
						partie.getProposition()[partie.getLigne()][b.getPosition()].setCouleur(0);
						getJeu(Integer.parseInt(component.getName()), detect);
					}

				} catch (ExceptionJeu ej) {
					traiter_err(ej);
				}

			} else {
				b.setVisible(false);
				partie.getProposition()[partie.getLigne()][b.getPosition()].setCouleur(0);
			}

		}

	}

	@Override
	//detection d'un composant
	public void mouseEntered(MouseEvent e) {

		Component component = e.getComponent();

		if (component instanceof Boule) {
			Boule b = (Boule) component;
			b.setOpaque(true);
			b.setOpaque(false);
			b.setIcon(vue.grasPion(Integer.parseInt(b.getName())));
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Component component = e.getComponent();

		if (component instanceof Boule) {
			Boule b = (Boule) component;
			b.setIcon(vue.mettrePion(Integer.parseInt(b.getName())));
			b.setOpaque(false);
		}
	}


	public int detectionv(MouseEvent e) {;
	for (int j=0; j<partie.getNbreColonne(); j++) {
		if (partie.getProposition()[partie.getLigne()][j].getBounds().intersects(e.getComponent().getBounds())) {
			return j;
		}

	}
	return 250;
	}

	@Override
	public void mouseDragged(MouseEvent e) {

		Component component = e.getComponent();		

		if ((component instanceof Boule) && (component.getName() != "0")) {

			Boule b = (Boule)component;
			b.setBounds(b.getX()+e.getX()-5,b.getY()-5+e.getY(),50,50);
		}
	}


	@Override
	public void mouseMoved(MouseEvent e) {

	}

	public void getJeu(int nbr, int detect) throws ExceptionJeu {

		if (partie.getNbreCouleur()<nbr) {
			throw new ExceptionJeu(ExceptionJeu.typeErreur.NBR_ELEVE);
		}

		partie.getJeu(nbr,detect);
	}

	private void traiter_err(ExceptionJeu e) {
		switch (e.getErreur()) {

		case NBR_ELEVE:
			//choix couleur incorrect
			vue.nombreEleve();
			break;
		}

	}
}