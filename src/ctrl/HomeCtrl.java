
package ctrl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import model.Partie;
import commun.Son;
import vue.FenetreConfiguration;
import vue.FenetreCredits;
import vue.JeuGraphique;

/**
 *
 * @author Jérôme Fafchamps
 * @version 1.0
 * 
 */

public class HomeCtrl implements ActionListener, MouseListener {



	public HomeCtrl() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String source = e.getActionCommand();

		if(source == "Jouer"){

			Partie modele = new Partie();
			//colonne/nbredecouleur/essai///
			//	modele.configuration(4,5,6);
			modele.configuration();
			JeuGraphique vue = new JeuGraphique();
			vue.setModele(modele);


		}  	else if (source == "Credit"){
			FenetreCredits f = new FenetreCredits();
		}	else if (source == "Configuration"){
			FenetreConfiguration fc = new FenetreConfiguration();
		}	else if (source == "Fermer"){
			System.exit(0);
		}	

	}


	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

		Son player = new Son("blip1.wav");
		player.start(); 
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
